//
//  ViewController.m
//  tryCallEnvBao
//
//  Created by 谢 子超 on 16/8/16.
//  Copyright © 2016年 test. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)callEnvBaoButtonTapped:(id)sender {
    NSString *deviceId = ((UIButton *)sender).titleLabel.text;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"envbao3:///thirdparty/57b2be160a2b580063371403/chooseStorage?deviceId=%@&scheme=linglong-dingdong", deviceId]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    } else {
        //需要安装最新版环境点评App
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"需要安装最新版环境点评App" delegate:nil cancelButtonTitle:@"好" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
