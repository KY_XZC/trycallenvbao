//
//  AppDelegate.m
//  tryCallEnvBao
//
//  Created by 谢 子超 on 16/8/16.
//  Copyright © 2016年 test. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    return [self handleURL:url sourceApplication:nil annotation:nil];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [self handleURL:url sourceApplication:sourceApplication annotation:annotation];
}

-(NSDictionary *)queryDictionaryFromQueryString:(NSString*)queryString
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *param in [queryString componentsSeparatedByString:@"&"]) {
        NSArray *parts = [param componentsSeparatedByString:@"="];
        if([parts count] < 2) continue;
        [params setObject:[parts objectAtIndex:1] forKey:[parts objectAtIndex:0]];
    }
    return params;
}

-(BOOL)handleURL:(NSURL*)url sourceApplication:(NSString *)sourceApplication
      annotation:(id)annotation {
    NSDictionary *query = [self queryDictionaryFromQueryString:url.query];
    int resultcode = [[query objectForKey:@"resultcode"] intValue];
    int status = [[query objectForKey:@"status"] intValue];
    NSString *statusdesc = [query objectForKey:@"statusdesc"];
    statusdesc = [statusdesc stringByRemovingPercentEncoding];
    NSString *resultString = [query objectForKey:@"result"];
    resultString = [resultString stringByRemovingPercentEncoding];
    NSLog(@"resultcode: %d", resultcode);
    NSLog(@"status: %d", status);
    NSLog(@"statusdesc: %@", statusdesc);
    NSLog(@"result: %@", resultString);
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
